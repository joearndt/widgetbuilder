# Widget Builder
The purpose of this code is demonstrate the power of mocking.
In this repo you will find a 'widget' builder and a few branches that demonstrate different types of tests. Most of the test utilize mocking and the various feature of the Moq library.

For more information on how this code is used, checkout the accompanying blog post, found here: https://ssiagvance.atlassian.net/l/c/8UC49PkP