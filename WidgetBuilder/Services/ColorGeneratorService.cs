using System;
using System.Collections.Generic;
using WidgetBuilder.Services.Interfaces;

namespace WidgetBuilder.Services
{
    public class ColorGeneratorService : IColorGenerator
    {
        private readonly Random _rand = new Random();
        
        public string GenerateColor()
        {
            var colors = new List<string> {"Red", "Green", "Blue"};
            return colors[_rand.Next(colors.Count)];
        }
    }
}