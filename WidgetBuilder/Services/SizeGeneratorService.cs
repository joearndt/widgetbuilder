using System;
using System.Collections.Generic;
using WidgetBuilder.Services.Interfaces;

namespace WidgetBuilder.Services
{
    public class SizeGeneratorService : ISizeGenerator
    {
        private readonly Random _rand = new Random();
        
        public int GenerateSize()
        {
            var sizes = new List<int> {1,2,3,4,5};
            return _rand.Next(sizes.Count);
        }
    }
}