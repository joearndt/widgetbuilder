using WidgetBuilder.Model;
using WidgetBuilder.Services.Interfaces;

namespace WidgetBuilder.Services
{
    public class WidgetBuilderService
    {
        private readonly IColorGenerator _colorGenerator;
        private readonly INameGenerator _nameGenerator;
        private readonly ISizeGenerator _sizeGenerator;

        public WidgetBuilderService(IColorGenerator colorGenerator, INameGenerator nameGenerator, ISizeGenerator sizeGenerator)
        {
            _colorGenerator = colorGenerator;
            _nameGenerator = nameGenerator;
            _sizeGenerator = sizeGenerator;
        }
        
        public Widget Build()
        {
            return new Widget
            {
                Color = _colorGenerator.GenerateColor(),
                Name = _nameGenerator.GenerateName(),
                Size = _sizeGenerator.GenerateSize()
            };
        }
    }
}