using System;
using System.Collections.Generic;
using WidgetBuilder.Services.Interfaces;

namespace WidgetBuilder.Services
{
    public class NameGeneratorService : INameGenerator
    {
        private readonly Random _rand = new Random();
        
        public string GenerateName()
        {
            var names = new List<string> {"Glossy Widget", "Matte Widget"};
            return names[_rand.Next(names.Count)];
        }
    }
}