namespace WidgetBuilder.Services.Interfaces
{
    public interface INameGenerator
    {
        public string GenerateName();
    }
}