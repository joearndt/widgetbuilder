namespace WidgetBuilder.Services.Interfaces
{
    public interface ISizeGenerator
    {
        public int GenerateSize();
    }
}