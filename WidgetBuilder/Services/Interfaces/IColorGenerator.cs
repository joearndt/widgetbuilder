namespace WidgetBuilder.Services.Interfaces
{
    public interface IColorGenerator
    {
        public string GenerateColor();
    }
}