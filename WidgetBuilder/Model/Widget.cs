namespace WidgetBuilder.Model
{
    public class Widget
    {
        public string Name { get; set; }
        public string Color { get; set; }
        public int Size { get; set; }
    }
}