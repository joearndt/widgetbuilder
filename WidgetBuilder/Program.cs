﻿using System;
using Newtonsoft.Json;
using WidgetBuilder.Services;
using WidgetBuilder.Services.Interfaces;

namespace WidgetBuilder
{
    public static class Program
    {
        public static void Main()
        {
            IColorGenerator colorService = new ColorGeneratorService();
            INameGenerator nameService = new NameGeneratorService();
            ISizeGenerator sizeService = new SizeGeneratorService();

            var widgetBuilder = new WidgetBuilderService(colorService, nameService, sizeService);
            
            for (var count = 0; count < 3; count++)
            {
                var widget = widgetBuilder.Build();
                
                Console.WriteLine(JsonConvert.SerializeObject(widget));
            }
        }
    }
}